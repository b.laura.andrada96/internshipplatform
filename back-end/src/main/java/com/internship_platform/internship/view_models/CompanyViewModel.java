package com.internship_platform.internship.view_models;

import com.internship_platform.internship.models.Company;

public class CompanyViewModel {
    private String name;
    private String email;
    private String password;
    private String address;
    private String description;
    private String phone;
    private String link;
    private String cui;

    public CompanyViewModel() {
    }
    public CompanyViewModel(Company company) {

        this.name=company.getName();
        this.cui=company.getCui();
        this.email=company.getEmail();
        this.password=company.getPassword();
        this.description=company.getDescription();
        this.link=company.getLink();
        this.phone=company.getPhone();
        this.address=company.getAddress();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }
}

package com.internship_platform.internship.view_models;

import com.internship_platform.internship.models.SiteAdmin;

public class SiteAdminViewModel {
    private String email;
    private String password;

    public SiteAdminViewModel() {
    }

    public SiteAdminViewModel(SiteAdmin admin) {
        this.email=admin.getEmail();
        this.password=admin.getPassword();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

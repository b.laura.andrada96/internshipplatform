package com.internship_platform.internship.view_models;

import com.internship_platform.internship.models.Student;

public class StudentViewModel {
    private String name;
    private String email;
    private String password;
    private String university;
    private String specialization;
    private String faculty;
    private int age;
    private String city;
    private String phone;
    private String county;

    public StudentViewModel() {
    }

    public StudentViewModel(Student student){
        this.name=student.getName();
        this.email=student.getEmail();
        this.password=student.getPassword();
        this.university=student.getUniversity();
        this.specialization=student.getSpecialization();
        this.faculty=student.getFaculty();
        this.age=student.getAge();
        this.city=student.getCity();
        this.phone=student.getPhone();
        this.county=student.getCounty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
}

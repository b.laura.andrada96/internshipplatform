package com.internship_platform.internship.repositories;

import com.internship_platform.internship.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CompanyRepository extends JpaRepository<Company,Long> {
    Company findOneByEmail(String email);
}

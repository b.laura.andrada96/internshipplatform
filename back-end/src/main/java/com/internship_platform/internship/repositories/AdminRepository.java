package com.internship_platform.internship.repositories;

import com.internship_platform.internship.models.SiteAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AdminRepository extends JpaRepository<SiteAdmin,Long> {
    SiteAdmin findOneByEmail(String email);
}

package com.internship_platform.internship.repositories;

import com.internship_platform.internship.models.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface AnnouncementRepository extends JpaRepository<Announcement,Long> {
     List<Announcement> findAllByCompany_Id(Long companyId);

}

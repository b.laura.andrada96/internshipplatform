package com.internship_platform.internship.repositories;

import com.internship_platform.internship.models.Application;
import com.internship_platform.internship.models.ApplicationId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Application findApplicationByPk(ApplicationId applicationId);
}

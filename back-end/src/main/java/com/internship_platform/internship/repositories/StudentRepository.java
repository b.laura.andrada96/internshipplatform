package com.internship_platform.internship.repositories;

import com.internship_platform.internship.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface StudentRepository extends JpaRepository<Student,Long> {
    Student findOneByEmail(String email);
}

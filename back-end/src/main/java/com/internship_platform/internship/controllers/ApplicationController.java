package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.Announcement;
import com.internship_platform.internship.models.Application;
import com.internship_platform.internship.models.ApplicationStatus;
import com.internship_platform.internship.models.Student;
import com.internship_platform.internship.repositories.AnnouncementRepository;
import com.internship_platform.internship.repositories.ApplicationRepository;
import com.internship_platform.internship.repositories.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class ApplicationController {
    private static ApplicationRepository applicationRepository;
    private static AnnouncementRepository announcementRepository;
    private static StudentRepository studentRepository;

    public ApplicationController(ApplicationRepository applicationRepository, AnnouncementRepository announcementRepository, StudentRepository studentRepository) {
        this.applicationRepository = applicationRepository;
        this.announcementRepository = announcementRepository;
        this.studentRepository = studentRepository;
    }

    @PutMapping(path = "/applications", consumes = {"application/json"})
    public ResponseEntity<String> addApplication(@RequestBody Application application) {
        Application existentApplication = applicationRepository.findApplicationByPk(application.getPk());
        if(existentApplication != null) {
            return new ResponseEntity<>("Application already exists", HttpStatus.BAD_REQUEST);
        }

        Announcement existentAnnouncement = announcementRepository.getOne(application.getAnnouncement().getAnnouncementId());
        if(existentAnnouncement == null) {
            return new ResponseEntity<>("Announcement does not exist", HttpStatus.BAD_REQUEST);
        }

        Student existentStudent = studentRepository.getOne(application.getStudent().getStudentId());
        if(existentStudent == null) {
            return new ResponseEntity<>("Student does not exist", HttpStatus.BAD_REQUEST);
        }

        applicationRepository.save(application);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/applications", consumes = {"application/json"})
    public ResponseEntity<String> removeApplication(@RequestBody Application application) {
        Application existentApplication = applicationRepository.findApplicationByPk(application.getPk());
        if(existentApplication == null) {
            return new ResponseEntity<>("Application does not exist", HttpStatus.BAD_REQUEST);
        }

        applicationRepository.delete(application);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/applications/accept", consumes = {"application/json"})
    public ResponseEntity<String> acceptApplication(@RequestBody Application application) {
        return setApplicationStatus(application, ApplicationStatus.ACCEPTED);
    }

    @PostMapping(path = "/applications/reject", consumes = {"application/json"})
    public ResponseEntity<String> rejectApplication(@RequestBody Application application) {
        return setApplicationStatus(application, ApplicationStatus.REJECTED);
    }

    private ResponseEntity<String> setApplicationStatus(Application application, int status) {
        Application existentApplication = applicationRepository.findApplicationByPk(application.getPk());
        if(existentApplication == null) {
            return new ResponseEntity<>("Application does not exist", HttpStatus.BAD_REQUEST);
        }

        existentApplication.setStatus(status);
        applicationRepository.save(existentApplication);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.Announcement;
import com.internship_platform.internship.models.Company;
import com.internship_platform.internship.models.UserRole;
import com.internship_platform.internship.repositories.CompanyRepository;
import com.internship_platform.internship.utils.JWTUtils;
import com.internship_platform.internship.view_models.CompanyViewModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class CompanyController {
    private CompanyRepository repository;

    public CompanyController(CompanyRepository repository) {
        this.repository = repository;
    }

    @PostMapping(path = "/companies", consumes = {"application/json"})
    public ResponseEntity<Company> addCompany(@RequestBody Company company, @CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(repository.save(company), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/companies", consumes = {"application/json"})
    public ResponseEntity<Company> deleteCompany(@RequestBody Company company, @CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || (JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        repository.delete(company);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/companies", consumes = {"application/json"})
    public ResponseEntity<Company> updateCompany(@RequestBody CompanyViewModel company, @CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);

        Optional<Company> oldCompany = repository.findById(id);
        if (!oldCompany.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Company company1 = oldCompany.get();
        company1.setName(company.getName());
        company1.setCui(company.getCui());
        company1.setAddress(company.getAddress());
        company1.setEmail(company.getEmail());
        company1.setPassword(company.getPassword());
        company1.setPhone(company.getPhone());
        company1.setDescription(company.getDescription());
        company1.setLink(company.getLink());

        repository.save(company1);
        return new ResponseEntity<>(company1, HttpStatus.OK);
    }

    @GetMapping("/companies")
    public ResponseEntity<List<Company>> getCompanies(@CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/companies/{companyId}")
    public ResponseEntity<Company> getCompany(@CookieValue("token") String cookie, @PathVariable Long companyId) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Company> company = repository.findById(companyId);
        return company.map(a1 -> new ResponseEntity<>(a1, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/companies/company/{companyId}/announcements")
    public ResponseEntity<List<Announcement>> getCompanyAnnouncements(@CookieValue("token") String cookie, @PathVariable Long companyId) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Company> company = repository.findById(companyId);
        if (company.isPresent()) {
            List<Announcement> announcements = repository.findById(companyId).get().getAnnouncements();
            return new ResponseEntity<>(announcements, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/companies/company/{companyId}/mostwanted")
    public ResponseEntity<List<Announcement>> getCompanyMostWantedAnnouncements(@CookieValue("token") String cookie, @PathVariable Long companyId) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Company> company = repository.findById(companyId);
        if (company.isPresent()) {

            List<Announcement> mostWanted = repository.findById(companyId).get().getAnnouncements();
            mostWanted.sort((o1, o2) -> o2.getApplications().size() - o1.getApplications().size());
            return new ResponseEntity<>(mostWanted, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/companies/email/{email}")
    public ResponseEntity<Company> getCompanyByEmail(@CookieValue("token") String cookie, @PathVariable String email) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN) || JWTUtils.getRole(cookie).equals(UserRole.COMPANY))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Company company = repository.findOneByEmail(email);
        if (Objects.nonNull(company)) {
            if (!JWTUtils.getUserId(cookie).equals(company.getId())) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(company, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/company")
    public ResponseEntity<CompanyViewModel> getCompanyByToken(@CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.COMPANY)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);

        Optional<Company> company = repository.findById(id);
        if (company.isPresent()) {
            CompanyViewModel companyViewModel = new CompanyViewModel((Company) company.get());
            return new ResponseEntity<>(companyViewModel, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}


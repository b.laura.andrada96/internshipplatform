package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.Student;
import com.internship_platform.internship.models.UserRole;
import com.internship_platform.internship.repositories.StudentRepository;
import com.internship_platform.internship.utils.JWTUtils;
import com.internship_platform.internship.view_models.StudentViewModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class StudentController {
    private StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @PostMapping(path = "/students", consumes = {"application/json"})
    public ResponseEntity<Student> addStudent(@RequestBody Student student, @CookieValue("token") String cookie) {

        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.CREATED);
    }

    @PutMapping(path = "/students", consumes = {"application/json"})
    public ResponseEntity<Student> updateStudent(@RequestBody StudentViewModel student, @CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.STUDENT))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if(optionalStudent.isPresent()){
            Student student1=optionalStudent.get();
            student1.setName(student.getName());
            student1.setEmail(student.getEmail());
            student1.setPassword(student.getPassword());
            student1.setPhone(student.getPhone());
            student1.setCity(student.getCity());
            student1.setCounty(student.getCounty());
            student1.setUniversity(student.getUniversity());
            student1.setFaculty(student.getFaculty());
            student1.setSpecialization(student.getSpecialization());
            student1.setAge(student.getAge());


            return new ResponseEntity<>(studentRepository.save(student1), HttpStatus.CREATED);
        }


        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/students")
    public ResponseEntity<List<Student>> getAll(@CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("students/{studentId}")
    public ResponseEntity<Student> findById(@CookieValue("token") String cookie, @PathVariable Long studentId) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Student> student = studentRepository.findById(studentId);
        return student.map(student1 -> new ResponseEntity<>(student1, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/students/email/{email}")
    public ResponseEntity<Student> getStudentByEmail(@CookieValue("token") String cookie, @PathVariable String email) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Student student = studentRepository.findOneByEmail(email);
        if (Objects.nonNull(student)) {
            if (!JWTUtils.getUserId(cookie).equals(student.getStudentId())) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(student, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/student")
    public ResponseEntity<StudentViewModel> getStudentByToken(@CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.STUDENT)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);

        Optional student = studentRepository.findById(id);
        if (student.isPresent()) {
            StudentViewModel studentViewModel = new StudentViewModel((Student)student.get());
            return new ResponseEntity<>(studentViewModel, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.SiteAdmin;
import com.internship_platform.internship.models.UserRole;
import com.internship_platform.internship.repositories.AdminRepository;
import com.internship_platform.internship.utils.JWTUtils;
import com.internship_platform.internship.view_models.SiteAdminViewModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class AdminController {
    private AdminRepository adminRepository;

    public AdminController(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }
    @GetMapping("/admins/email/{email}")
    public ResponseEntity<SiteAdmin> getAdminByEmail(@CookieValue("token") String cookie, @PathVariable String email) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)) ) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        SiteAdmin siteAdmin = adminRepository.findOneByEmail(email);

        if (Objects.nonNull(siteAdmin)) {
            if (!JWTUtils.getUserId(cookie).equals(siteAdmin.getIdAdmin())) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(siteAdmin, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(path = "/admins", consumes = {"application/json"})
    public ResponseEntity<SiteAdmin> updateAdmin(@RequestBody SiteAdminViewModel siteAdmin, @CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);
        Optional<SiteAdmin> optionalSiteAdmin = adminRepository.findById(id);
        if(optionalSiteAdmin.isPresent()){
            SiteAdmin siteAdmin1 = optionalSiteAdmin.get();
            siteAdmin1.setEmail(siteAdmin.getEmail());
            siteAdmin1.setPassword(siteAdmin.getPassword());

            return new ResponseEntity<>(adminRepository.save(siteAdmin1), HttpStatus.CREATED);
        }


        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/admin")
    public ResponseEntity<SiteAdminViewModel> getSiteAdminByToken(@CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Long id = JWTUtils.getUserId(cookie);

        Optional<SiteAdmin> siteAdmin = adminRepository.findById(id);
        if (siteAdmin.isPresent()) {
            SiteAdminViewModel siteAdminViewModel = new SiteAdminViewModel((SiteAdmin) siteAdmin.get());
            return new ResponseEntity<>(siteAdminViewModel, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}

package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.Announcement;
import com.internship_platform.internship.models.UserRole;
import com.internship_platform.internship.repositories.AnnouncementRepository;
import com.internship_platform.internship.utils.JWTUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class AnnouncementController {
    private AnnouncementRepository announcementRepository;

    public AnnouncementController(AnnouncementRepository announcementRepository) {
        this.announcementRepository = announcementRepository;
    }

    @PostMapping(path = "/announcements", consumes = {"application/json"})
    public ResponseEntity<Announcement> addAnnouncement(@RequestBody Announcement announcement, @CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.COMPANY)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if(!JWTUtils.getUserId(cookie).equals(announcement.getCompany().getId().toString())){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        announcement.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
        announcement.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
        return new ResponseEntity<>(announcementRepository.save(announcement), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/announcements", consumes = {"application/json"})
    public ResponseEntity<Announcement> deleteAnnouncement(@RequestBody Announcement announcement, @CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.COMPANY)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if(!JWTUtils.getUserId(cookie).equals(announcement.getCompany().getId().toString())){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        announcementRepository.delete(announcement);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/announcements", consumes = {"application/json"})
    public ResponseEntity<Announcement> updateAnnouncement(@RequestBody Announcement announcement, @CookieValue("token") String cookie) {
        if (!JWTUtils.getRole(cookie).equals(UserRole.COMPANY)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if(!JWTUtils.getUserId(cookie).equals(announcement.getCompany().getId().toString())){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if(Objects.isNull(announcement.getAnnouncementId())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<Announcement> oldAnnouncement = announcementRepository.findById(announcement.getAnnouncementId());
        if (!oldAnnouncement.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        announcement.setAnnouncementId(oldAnnouncement.get().getAnnouncementId());
        announcement.setCreationDate(oldAnnouncement.get().getCreationDate());
        announcement.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
        announcementRepository.save(announcement);
        return new ResponseEntity<>(announcement, HttpStatus.OK);
    }


    @GetMapping("/announcements")
    public ResponseEntity<List<Announcement>> getAnnouncements(@CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(announcementRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/announcements/{announcementId}")
    public ResponseEntity<Announcement> getAnnouncement(@CookieValue("token") String cookie, @PathVariable Long announcementId) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Announcement> announcement = announcementRepository.findById(announcementId);
        return announcement.map(a1 -> new ResponseEntity<>(a1, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/announcements/mostwanted")
    public ResponseEntity<List<Announcement>> getMostWantedAnnouncements(@CookieValue("token") String cookie) {
        if (!(JWTUtils.getRole(cookie).equals(UserRole.COMPANY) || JWTUtils.getRole(cookie).equals(UserRole.STUDENT) || JWTUtils.getRole(cookie).equals(UserRole.SITE_ADMIN))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        List<Announcement> mostWanted = announcementRepository.findAll();
        mostWanted.sort((o1, o2) -> o2.getApplications().size()-o1.getApplications().size());
        return new ResponseEntity<>(mostWanted, HttpStatus.OK);
    }

}


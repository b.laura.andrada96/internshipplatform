package com.internship_platform.internship.controllers;

import com.internship_platform.internship.models.*;
import com.internship_platform.internship.repositories.AdminRepository;
import com.internship_platform.internship.repositories.CompanyRepository;
import com.internship_platform.internship.repositories.StudentRepository;
import com.internship_platform.internship.utils.JWTUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
public class AccountController {
    private static StudentRepository studentRepository;
    private static AdminRepository adminRepository;
    private static CompanyRepository companyRepository;

    public AccountController(StudentRepository studentRepository, AdminRepository adminRepository, CompanyRepository companyRepository) {
        this.studentRepository = studentRepository;
        this.adminRepository = adminRepository;
        this.companyRepository = companyRepository;
    }

    @PostMapping(path = "/login", consumes = {"application/json"})
    public ResponseEntity<String> loginUser(@Valid @RequestBody User user, HttpServletResponse response) {
        Student student = studentRepository.findOneByEmail(user.getEmail());
        Cookie cookie = null;
        if (student != null && user.getPassword().equals(student.getPassword())) {
            cookie = new Cookie("token", JWTUtils.encode(student.getStudentId().toString(), student.getEmail(), UserRole.STUDENT.toString()));
            response.addCookie(cookie);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        Company company = companyRepository.findOneByEmail(user.getEmail());
        if (company != null && user.getPassword().equals(company.getPassword())) {
            cookie = new Cookie("token", JWTUtils.encode(company.getId().toString(), company.getEmail(), UserRole.COMPANY.toString()));
            response.addCookie(cookie);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        SiteAdmin siteAdmin = adminRepository.findOneByEmail(user.getEmail());
        if (siteAdmin != null && user.getPassword().equals(siteAdmin.getPassword())) {
            cookie = new Cookie("token", JWTUtils.encode(siteAdmin.getIdAdmin().toString(), siteAdmin.getEmail(), UserRole.SITE_ADMIN.toString()));
            response.addCookie(cookie);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        cookie.setPath("/");
        cookie.setHttpOnly(false);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping(path = "sign-up/company", consumes = {"application/json"})
    public ResponseEntity<String> signUpCompany(@Valid @RequestBody Company company, HttpServletResponse response) {
        Company existentCompany = companyRepository.findOneByEmail(company.getEmail());
        if (existentCompany != null) {
            return new ResponseEntity<>("A company with the same email already exists", HttpStatus.BAD_REQUEST);
        }
        companyRepository.save(company);
        Cookie cookie = new Cookie("token", JWTUtils.encode(company.getId().toString(), company.getEmail(), UserRole.COMPANY.toString()));
        cookie.setPath("/");
        cookie.setHttpOnly(false);
        response.addCookie(cookie);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "sign-up/student", consumes = {"application/json"}, method = RequestMethod.PUT)
    public ResponseEntity<String> signUpStudent(@Valid @RequestBody Student student, HttpServletResponse response) {
        Student existentStudent = studentRepository.findOneByEmail(student.getEmail());
        if (existentStudent != null) {
            return new ResponseEntity<>("A student with the same email already exists", HttpStatus.BAD_REQUEST);
        }
        studentRepository.save(student);
        Cookie cookie = new Cookie("token", JWTUtils.encode(student.getStudentId().toString(), student.getEmail(), UserRole.STUDENT.toString()));
        cookie.setPath("/");
        cookie.setHttpOnly(false);
        response.addCookie(cookie);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/logout")
    public ResponseEntity<String> logoutUser(HttpServletResponse response) {
        Cookie tokenCookie = new Cookie("token", "");
        tokenCookie.setMaxAge(0);
        response.addCookie(tokenCookie);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/roles")
    public ResponseEntity<String> getCompanyByEmail(@CookieValue("token") String cookie) {
        return new ResponseEntity<>(JWTUtils.getRole(cookie).toString(),HttpStatus.OK);
    }

}



package com.internship_platform.internship.utils;

import com.internship_platform.internship.models.UserRole;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.UUID;

public class JWTUtils {
    private static final String secret = "secretafhasfy7831ryoihkwasnljfhih9138yfefsdvsdgfsd@!$#%%^UYTHFBASDDHFJERSgdoghwpo2i3gweosidslkhf9p382qwoai";

    public static String encode(String id, String email, String role) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secret);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtBuilder builder = Jwts.builder().setId(UUID.randomUUID().toString())
                .claim("userId", id)
                .claim("email", email)
                .claim("role", role)
                .signWith(signingKey, signatureAlgorithm);
        return builder.compact();
    }

    public static Claims decode(String cookie) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(secret))
                .parseClaimsJws(cookie).getBody();
    }

    public static UserRole getRole(String cookie) {
        Claims claims = decode(cookie);
        return UserRole.valueOf((String) claims.get("role"));
    }

    public static Long getUserId(String cookie){
        Claims claims = decode(cookie);
        return Long.valueOf((String)claims.get("userId"));
    }

}

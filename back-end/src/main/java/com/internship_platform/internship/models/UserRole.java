package com.internship_platform.internship.models;

public enum UserRole {
    SITE_ADMIN,
    STUDENT,
    COMPANY
}

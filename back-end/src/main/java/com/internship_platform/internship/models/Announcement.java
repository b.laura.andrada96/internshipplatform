package com.internship_platform.internship.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "announcements")
@Table(name = "announcements")
@EntityListeners(AuditingEntityListener.class)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "announcementId",
        scope = Announcement.class)
public class Announcement implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "announcement_id", unique = true, nullable = false)
    private Long announcementId;


    @JsonManagedReference
    @ManyToOne
    @JoinColumn(
            name = "company_id",
            referencedColumnName = "company_id"
    )
    private Company company;

    private String title;
    private String description;
    private String department;
    private String field;
    private int noAvailableSpots;
    private boolean payed;
    private String requirements;
    @JsonFormat(pattern = "YYYY-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @JsonFormat(pattern = "YYYY-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @JsonFormat(pattern = "YYYY-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date deadline;
    @JsonFormat(pattern = "YYYY-MM-dd-hh-mm", timezone = "Europe/Bucharest")
    private Timestamp creationDate;
    @JsonFormat(pattern = "YYYY-MM-dd-hh-mm", timezone = "Europe/Bucharest")
    private Timestamp updateDate;
    private int hours;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.announcement")
    private List<Application> applications = new ArrayList<>();

    public Announcement() {
    }

    public Announcement(Long announcementId, Company company, String title) {
        this.announcementId = announcementId;
        this.company = company;
    }

    public Long getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(Long announcementId) {
        this.announcementId = announcementId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getNoAvailableSpots() {
        return noAvailableSpots;
    }

    public void setNoAvailableSpots(int noAvailableSpots) {
        this.noAvailableSpots = noAvailableSpots;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }


    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    public void addApplication(Application application) {
        this.applications.add(application);
    }
}

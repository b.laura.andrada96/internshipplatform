package com.internship_platform.internship.models;


/*-----------------------------------------
    0  -  Applications pending response
    1  -  Applications rejected
    2  -  Applications accepted
 -----------------------------------------*/
public class ApplicationStatus {
    public static final int SUBMITTED = 0;
    public static final int REJECTED = 1;
    public static final int ACCEPTED = 2;
}

package com.internship_platform.internship.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "applications")
@AssociationOverrides({
        @AssociationOverride(name = "pk.student",
                joinColumns = @JoinColumn(name = "student_id")),
        @AssociationOverride(name = "pk.announcement",
                joinColumns = @JoinColumn(name = "announcement_id")) })
@EntityListeners(AuditingEntityListener.class)
public class Application implements java.io.Serializable {

    private ApplicationId pk = new ApplicationId();
    private int status;

    public Application() {
    }

    @EmbeddedId
    public ApplicationId getPk() {
        return pk;
    }

    public void setPk(ApplicationId pk) {
        this.pk = pk;
    }

    @Transient
    public Student getStudent() {
        return getPk().getStudent();
    }

    public void setStudent(Student student) {
        getPk().setStudent(student);
    }

    @Transient
    public Announcement getAnnouncement() {
        return getPk().getAnnouncement();
    }

    public void setAnnouncement(Announcement announcement) {
        getPk().setAnnouncement(announcement);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Application that = (Application) o;

        if (getPk() != null ? !getPk().equals(that.getPk())
                : that.getPk() != null)
            return false;

        return true;
    }

    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }
}

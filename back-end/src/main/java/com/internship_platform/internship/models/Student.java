package com.internship_platform.internship.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "students")
@EntityListeners(AuditingEntityListener.class)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "studentId",
        scope = Student.class)
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "student_id", unique = true, nullable = false)
    private Long studentId;
    private String name;
    @Size(min = 1, message = "Email is empty")
    @Column(name = "email", unique = true, nullable = false, length = 20)
    private String email;
    @Size(min = 1, message = "Password is empty")
    private String password;
    private String university;
    private String specialization;
    private String faculty;
    @Min(10)
    @Max(100)
    private int age;
    private String city;
    private String county;
    private String phone;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.student", cascade = CascadeType.ALL)
    @JsonBackReference
    private List<Application> applications = new ArrayList<>();

    public Student() {
    }

    public Student(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Student(String email, String password, String university) {
        this.email = email;
        this.password = password;
        this.university = university;
    }

    public Student(String email, String password, String university, String specialization, String faculty, int age, String city, String phone) {
        this.email = email;
        this.password = password;
        this.university = university;
        this.specialization = specialization;
        this.faculty = faculty;
        this.age = age;
        this.city = city;
        this.phone = phone;

    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    public void addApplication(Application application) {
        this.applications.add(application);
    }

    public Student(Long studentId, String email, String password) {
        this.studentId = studentId;
        this.email = email;
        this.password = password;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
}

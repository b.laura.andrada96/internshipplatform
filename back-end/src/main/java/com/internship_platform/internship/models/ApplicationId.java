package com.internship_platform.internship.models;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ApplicationId implements java.io.Serializable {

    private Student student;
    private Announcement announcement;

    @ManyToOne
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @ManyToOne
    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApplicationId that = (ApplicationId) o;

        if (student != null ? !student.equals(that.student) : that.student != null) return false;
        if (announcement != null ? !announcement.equals(that.announcement) : that.announcement != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (student != null ? student.hashCode() : 0);
        result = 31 * result + (announcement != null ? announcement.hashCode() : 0);
        return result;
    }

}
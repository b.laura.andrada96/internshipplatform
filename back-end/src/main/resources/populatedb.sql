use internship_platform

INSERT INTO students (age, city, email, faculty, name, password, phone, specialization, university) VALUES
(25, 'Cluj-Napoca', 'email@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email1@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email2@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email3@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email4@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email5@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email6@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email7@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email8@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email9@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB'),
(25, 'Cluj-Napoca', 'email10@student.com', 'UBB', 'Name1', 'password123', '123', 'Mate-info', 'UBB')


INSERT INTO companies (address, email, name, password) VALUES
('addr1', 'email@company.com', UBB, 'name', 'password123'),
('addr1', 'email1@company.com', UBB, 'name', 'password123'),
('addr1', 'email2@company.com', UBB, 'name', 'password123'),
('addr1', 'email3@company.com', UBB, 'name', 'password123'),
('addr1', 'email4@company.com', UBB, 'name', 'password123'),
('addr1', 'email5@company.com', UBB, 'name', 'password123'),
('addr1', 'email6@company.com', UBB, 'name', 'password123')


INSERT INTO announcements (no_available_spots, payed, company_id) VALUES
(1, 1, 1),
(14, 1, 2),
(12, 0, 3),
(1, 1, 4),
(5, 1, 5),
(2, 0, 6),
(1, 0, 7)

INSERT INTO applications (status, announcement_id, student_id) VALUES
(0, 1, 1),
(0, 2, 1)


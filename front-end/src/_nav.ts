export default{

    STUDENT:
        [{
            text: 'Dashboard',
            url: 'dashboard',
            icon: 'cui-dashboard',
        },
            {
                text: 'Vezi profil',
               url: 'student_profile/view',
                icon: 'cui-info',
            },
            {
                text: 'Editeaza Profil',
               url: 'student_profile/edit',
                icon: 'cui-pencil',
            },{
                text: 'MyApplications',
               url: 'myapplications',
                icon: 'icon-pin',
            },{
                text: 'Announcements',
                url: 'announcements',
                icon: 'icon-list',
            },


            {
                text: 'Companies',
                url: 'companies',
                icon: 'icon-briefcase',
            },
            {
                text: 'Statistics',
                url: 'statistics',
                icon: 'icon-pie-chart',
            }


        ],
    COMPANY:[
        {
            text: 'Dashboard',
            url: 'dashboard',
            icon: 'cui-dashboard',
        },{
            text: 'Profile',
            url: 'company_profile/view',
            icon: 'cui-info',
        },
        {
            text: 'EditProfile',
            url: 'company_profile/edit',
            icon: 'cui-pencil',
        },
        {text: 'MyApplicants',
        url: 'myapplicants',
        icon: 'icon-people',
    },{
        text: 'Announcements',
        url: 'announcements',
        icon: 'icon-list',
    },


        {
            text: 'Companies',
            url: 'companies',
            icon: 'icon-briefcase',
        },
        {
            text: 'Statistics',
            url: 'statistics',
            icon: 'icon-pie-chart',
        }],
    ADMIN:[
        {
            text: 'Dashboard',
            url: 'dashboard',
            icon: 'cui-dashboard',
        },{
            text: 'Profile',
            url: 'admin_profile/view',
            icon: 'cui-info',
        },
        {
            text: 'EditProfile',
            url: 'admin_profile/edit',
            icon: 'cui-pencil',
        },{
        text: 'Students',
        url: 'students',
        icon: 'icon-graduation',
    },{
        text: 'Announcements',
        url: 'announcements',
        icon: 'icon-list',
    },


        {
            text: 'Companies',
            url: 'companies',
            icon: 'icon-briefcase',
        },
        {
            text: 'Statistics',
            url: 'statistics',
            icon: 'icon-pie-chart',
        }]


};

import * as React from "react";

import {BodyLayout} from "./layout/BodyLayout";
import {Header} from "./layout/Header";
import {DefaultLayout} from "./layout/DefaultLayout";
import {LogIn} from "./main_components/Login";
import cookie from 'react-cookies';
import {AccountService} from "./services/AccountService";
import {Redirect,Router,BrowserRouter} from 'react-router-dom'
import {Body} from "./layout/Body";
import {StudentProfile} from "./main_components/profiles_pages/StudentProfile";
import {EditStudentProfile} from "./main_components/profiles_pages/EditStudentProfile";
import {SignUpStudent} from "./main_components/SignUpStudent";
import {EditCompanyProfile} from "./main_components/profiles_pages/EditCompanyProfile";
import {Profile} from "./main_components/sidepanel_pages/Profile";

export interface AppProps
{

}
interface AppState
{

    isLogged:boolean;
    role:string;
}

export class App extends React.Component<AppProps,AppState> {
    constructor(props)
    {
        super(props);
        this.state=
            {

                isLogged: false,
                role:''
            }

    }

    // componentDidMount(){
    //     if(cookie.load('token')){
    //
    //         AccountService.getUserRole().then((resp)=>{
    //             console.log('Role:'+resp.data);
    //             this.setState({role:resp.data, isLogged: true});
    //             // this.props.handleToParent(this.state.isLogged,this.state.role);
    //         });
    //
    //
    //     }
    // }
    //
    render() {


        return (
            <BrowserRouter>
                <Body/>
            </BrowserRouter>

        );
    }
}
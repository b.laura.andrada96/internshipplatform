import * as React from 'react';

import "../../node_modules/coreui-react/assets/css/style.css"
import "../styles/alignment.css";
import Alert from 'react-s-alert';
import update from 'react-addons-update';
import { VBox, HBox } from 'react-stylesheet';
import { ICompanyRegister } from '../models/ICompanyRegister';
import { AccountService } from '../services/AccountService';

export interface SignUpProps 
{
}

interface SignUpState {
    newUser: ICompanyRegister
    created: boolean
}

export class SignUpCompany extends React.Component<SignUpProps, SignUpState> 
{
    constructor(props: SignUpProps) {
        super(props);

        this.state =
            {
                newUser:
                    {
                        address: '',
                        email: '',
                        description: '',
                        link: '',
                        name: '',
                        password: '',
                        phone: '',
                        cui:''
                    },
                created: false,
            }
    }

    handleEmailChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { email: { $set: event.target.value } })
        });
    }
  
    handleAddressChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { address: { $set: event.target.value } })
        });
    }
 
    handlePasswordChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { password: { $set: event.target.value } })
        });
    }

    handleConfirmPasswordChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { confirmPassword: { $set: event.target.value } })
        });
    }

    handleSignUpPress(event: any) {
        console.log('signing up');
        AccountService.registerCompany(this.state.newUser).then((resp) => {
            this.setState({
                created: update(this.state.created, { $set: true } )
            });
            Alert.success("A new account has been created!", {
                position: 'top-right',
                effect: 'jelly'
            });
        });
    }

    handleNameChange(event : any) {
        this.setState({
            newUser: update(this.state.newUser, { name: { $set: event.target.value } })
        });
    }

    handlePhoneChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { phone: { $set: event.target.value } })
        });
    }

    handleLinkChange(event : any) {
        this.setState({
            newUser: update(this.state.newUser, { link: { $set: event.target.value } })
        });
    }

    handleCuiChange(event : any) {
        this.setState({
            newUser: update(this.state.newUser, { cui: { $set: event.target.value } })
        });
    }

    handleDescriptionChange(event: any){
        this.setState({
            newUser: update(this.state.newUser, { description: { $set: event.target.value } })
        });
    }

    render() {
        return (
            <form className="center">
            <VBox id="signup-container">
            <HBox class="hbox-align">
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">CUI</label>
                        <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="CUI" onChange={this.handleCuiChange.bind(this)}/>
                </div>
                </VBox>
                
                <VBox>
                <div className="form-group textbox-align">
                    <label htmlFor="formGroupExampleInput">Name</label>
                    <input type="text" className="form-control" id="formGroupExampleInput1" placeholder="Name" onChange={this.handleNameChange.bind(this)}/>
                </div>
                </VBox>
            </HBox>
            <HBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">Phone</label>
                        <input type="text" className="form-control" id="formGroupExampleInput8" placeholder="Phone" onChange={this.handlePhoneChange.bind(this)}/>
                </div>
                </VBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">Link</label>
                        <input type="text" className="form-control" id="formGroupExampleInput3" placeholder="City" onChange={this.handleLinkChange.bind(this)}/>
                </div>
                </VBox>
            </HBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Email</label>
                        <input type="text" className="form-control" id="formGroupExampleInput4" placeholder="Email" onChange={this.handleEmailChange.bind(this)}/>
                </div>
                
            </VBox>
            <VBox  class="vbox-size">
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Description</label>
                        <input type="text" className="form-control" id="formGroupExampleInput9" placeholder="Description" onChange={this.handleDescriptionChange.bind(this)}/>
                </div>
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Address</label>
                        <input type="text" className="form-control" id="formGroupExampleInput6" placeholder="Address" onChange={this.handleAddressChange.bind(this)}/>
                </div>
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Password</label>
                        <input type="password" className="form-control" id="formGroupExampleInput6" placeholder="Password" onChange={this.handlePasswordChange.bind(this)}/>
                </div>
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Confirm Password</label>
                        <input type="password" className="form-control" id="formGroupExampleInput7" placeholder="Confirm Password" onChange={this.handleConfirmPasswordChange.bind(this)}/>
                </div>
            </VBox>
            
            </VBox>
            <button type = "button" className="btn btn-primary btn-lg center-button" onClick={(event) => this.handleSignUpPress(event)}>Sign up</button>
            </form>
            
        );
    }
}

import * as React from "react";
import { VBox, HBox } from 'react-stylesheet';
import {Redirect} from 'react-router-dom';
import "../styles/alignment.css";
export interface SignUpProps
{
}

interface SignUpState {
    isCompanie:boolean,
    isStudent:boolean,
    back:boolean;
}

export class SignUp extends React.Component<SignUpProps, SignUpState> {
    constructor(props) {
        super(props);
        this.handleCompaniePressed= this.handleCompaniePressed.bind(this);
        this.handleStudentPressed= this.handleStudentPressed.bind(this);
        this.handleBackPressed= this.handleBackPressed.bind(this);
        this.state={isCompanie:false,isStudent:false,back:false};
    }

    handleCompaniePressed(event){
        this.setState({isCompanie:true,isStudent:false});
    }

    handleBackPressed(event){
        this.setState({isCompanie:false,isStudent:false,back:true});
    }

    handleStudentPressed(event){
        this.setState({isCompanie:false,isStudent:true});
    }
    render() {
        if(this.state.back){
            return(<Redirect to={'/'} />);
        }
        if(this.state.isCompanie){
            return(<Redirect to={'/signupCompany'} />);
        }
        else if(this.state.isStudent){
            return(<Redirect to={'/signupStudent'} />);
        }

        return (
            <div className={"startPage"}>
                <div className="centerStart">
                    <VBox>
                <button type = "button" className="btn btn-primary btn-lg center-buttonStart" onClick={(event) => this.handleCompaniePressed(event)}>Cont Companie</button>
                    </VBox>
                    <VBox>
                <button type = "button" className="btn btn-primary btn-lg center-buttonStart" onClick={(event) => this.handleCompaniePressed(event)}>Cont Student</button>
               </VBox>
                    <VBox>
                        <button type = "button" className="btn btn-primary btn-lg center-buttonStart" onClick={(event) => this.handleBackPressed(event)}>Inapoi</button>
                    </VBox>
                </div>
            </div>

        );
    }
}
import * as React from "react";
import {AccountService} from "../../services/AccountService";
import {ISiteAdmin} from "../../models/ISiteAdmin";
import update from 'react-addons-update';
import Alert from 'react-s-alert';
import "../../styles/alignment.css";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css';
import {
    Button,
    FormGroup,
    Input,
    Col,
    Label
} from 'reactstrap';

export interface EditAdminProfileProps
{
}

interface EditAdminProfileState {
    admin:ISiteAdmin
}


export class EditAdminProfile extends React.Component<EditAdminProfileProps, EditAdminProfileState> {
    constructor(props) {
        super(props);
        this.state={
            admin:{
                email:'',
                password:''
            }
        }
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);


    }

    componentDidMount(){
        AccountService.getAdmin().then((admin:ISiteAdmin) => {
            this.setState({
                admin:admin
            });
        });
    }

    handleUpdatePressed() {

        let valid = true;

        AccountService.updateAdmin(this.state.admin).then((resp) => {
            Alert.success("Modificari efectuate!", {
                position: 'top-right',
                effect: 'jelly'
            });
        });
    }

    handleEmailChange(event) {
        this.setState({
            admin: update(this.state.admin, {email: {$set: event.target.value}})
        });
    }

    handlePasswordChange(event) {
        this.setState({
            admin: update(this.state.admin, {password: {$set: event.target.value}})
        });
    }


    render() {
        return (
            <div>
                <h1>Editeaza Profil</h1>
                <div className={'centerEdit'}>
                    <FormGroup>
                        <Label >Email</Label>
                        <Input type="text" id="email"  onChange={this.handleEmailChange} value={this.state.admin.email}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Parola</Label>
                        <Input type="password" id="password"  onChange={this.handlePasswordChange} value={this.state.admin.password}/>
                    </FormGroup>

                    <Button type="submit" size="sm" color="primary" onClick={this.handleUpdatePressed}><i
                        className="fa fa-dot-circle-o" ></i> Salveaza</Button>
                </div>
            </div>

                    );
    }
}
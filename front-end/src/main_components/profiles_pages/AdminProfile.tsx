import * as React from "react";
import {ISiteAdmin} from "../../models/ISiteAdmin";
import {IStudentGet} from "../../models/IStudentGet";
import {AccountService} from "../../services/AccountService";
import { VBox, HBox } from 'react-stylesheet';
export interface AdminProfileProps
{
}

interface AdminProfileState {
    admin:ISiteAdmin
}

export class AdminProfile extends React.Component<AdminProfileProps, AdminProfileState> {
    constructor(props) {
        super(props);
        this.state={
            admin:{
                email:'',
                password:''
            }
        }

    }

    componentDidMount(){
        AccountService.getAdmin().then((admin:ISiteAdmin) => {
            this.setState({
                admin:admin
            });
        });
    }
    render() {
        return (
            <div><header>
                <h1>Profil Admin</h1>

            </header>
                <VBox className='info'>

                    <label >
                        <b>Email</b>
                    </label>

                    <label>
                        {this.state.admin.email}
                    </label>
                    <hr/>
                </VBox>
            </div>

        );
    }
}
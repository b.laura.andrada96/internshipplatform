import * as React from "react";
import {IStudentGet} from "../../models/IStudentGet";
import "../../styles/alignment.css";
//import "../../styles/scss/main.scss"
import {AccountService} from "../../services/AccountService";
import update from 'react-addons-update';
import Alert from 'react-s-alert';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css';
import {
    Button,
    FormGroup,
    Input,
    Col,
    Label
} from 'reactstrap';

export interface EditStudentProfileProps {
}

interface EditStudentProfileState {
    student: IStudentGet
}

export class EditStudentProfile extends React.Component<EditStudentProfileProps, EditStudentProfileState> {
    constructor(props) {
        super(props);
        this.state = {
            student: {
                name: '',
                email: '',
                password: '',
                university: '',
                specialization: '',
                faculty: '',
                age: 0,
                city: '',
                county: '',
                phone: ''
            }

        };
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleUpdatePressed = this.handleUpdatePressed.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleUniversityChange = this.handleUniversityChange.bind(this);
        this.handleSpecializationChange = this.handleSpecializationChange.bind(this);
        this.handleFacultyChange = this.handleFacultyChange.bind(this);
        this.handleCountyChange = this.handleCountyChange.bind(this);
        this.handleAgeChange=this.handleAgeChange.bind(this);



    }

    componentDidMount() {
        AccountService.getStudent().then((student: IStudentGet) => {
            this.setState({
                student: student
            });
        });
    }

    handleUpdatePressed() {

        let valid = true;
        console.log("in update student")
        AccountService.updateStudent(this.state.student).then((resp) => {
            Alert.success("Modificari efectuate!", {
                position: 'top-right',
                effect: 'jelly'
            });
        });
    }

    handleEmailChange(event) {
        this.setState({
            student: update(this.state.student, {email: {$set: event.target.value}})
        });
    }

    handlePasswordChange(event) {
        this.setState({
            student: update(this.state.student, {password: {$set: event.target.value}})
        });
    }

    handlePhoneChange(event) {
        this.setState({
            student: update(this.state.student, {phone: {$set: event.target.value}})
        });
    }
    handleAgeChange(event) {
        this.setState({
            student: update(this.state.student, {age: {$set: event.target.value}})
        });
    }



    handleNameChange(event) {
        this.setState({
            student: update(this.state.student, {name: {$set: event.target.value}})
        });
    }

    handleUniversityChange(event) {
        this.setState({
            student: update(this.state.student, {university: {$set: event.target.value}})
        });
    }

    handleFacultyChange(event) {
        this.setState({
            student: update(this.state.student, {faculty: {$set: event.target.value}})
        });
    }

    handleSpecializationChange(event) {
        this.setState({
            student: update(this.state.student, {specialization: {$set: event.target.value}})
        });
    }

    handleCityChange(event) {
        this.setState({
            student: update(this.state.student, {city: {$set: event.target.value}})
        });
    }
    handleCountyChange(event) {
        this.setState({
            student: update(this.state.student, {county: {$set: event.target.value}})
        });
    }


    render() {
        console.log(this.state.student[1]);
        return (

            <div>
                <h1>Editeaza Profil</h1>
                <div className={'centerEdit'}>
                    <FormGroup>
                        <Label >Nume</Label>
                        <Input type="text" id="name" onChange={this.handleNameChange} value={this.state.student.name}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Varsta</Label>
                        <Input type="number" id="age"  onChange={this.handleAgeChange} value={this.state.student.age}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Email</Label>
                        <Input type="text" id="email"  onChange={this.handleEmailChange} value={this.state.student.email}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Parola</Label>
                        <Input type="password" id="password"  onChange={this.handlePasswordChange} value={this.state.student.password}/>
                    </FormGroup>

                    <FormGroup>
                        <Label >Universitate</Label>
                        <Input type="text" id="universitate"  onChange={this.handleUniversityChange} value={this.state.student.university}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Facultate</Label>
                        <Input type="text" id="faculty"  onChange={this.handleFacultyChange} value={this.state.student.faculty}/>
                    </FormGroup>

                    <FormGroup>
                        <Label >Specializare</Label>
                        <Input type="text" id="specializare"  onChange={this.handleSpecializationChange} value={this.state.student.specialization}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Oras</Label>
                        <Input type="text" id="oras"  onChange={this.handleCityChange} value={this.state.student.city}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Judet</Label>
                        <Input type="text" id="judet"  onChange={this.handleCountyChange} value={this.state.student.county}/>
                    </FormGroup>
                    <FormGroup>
                        <Label >Telefon</Label>
                        <Input type="text" id="tel"  onChange={this.handlePhoneChange} value={this.state.student.phone}/>
                    </FormGroup>
                    <Button type="submit" size="sm" color="primary" onClick={this.handleUpdatePressed}><i
                        className="fa fa-dot-circle-o" ></i> Salveaza</Button>
                </div>
                </div>
        );
    }
}
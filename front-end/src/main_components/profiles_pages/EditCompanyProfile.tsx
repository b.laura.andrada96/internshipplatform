import * as React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css';
import '../../styles/alignment.css'
import update from 'react-addons-update';
import Alert from 'react-s-alert';
import {
    Button,
    FormGroup,
    Input,
    Col,
    Label
} from 'reactstrap';
import {ICompanyRegister} from "../../models/ICompanyRegister";
import {AccountService} from "../../services/AccountService";

export interface EditCompanyProfileProps {
}

interface EditCompanyProfileState {
    company: ICompanyRegister
}

export class EditCompanyProfile extends React.Component<EditCompanyProfileProps, EditCompanyProfileState> {
    constructor(props) {
        super(props);
        this.state =
            {
                company:
                    {
                        address: '',
                        email: '',
                        description: '',
                        link: '',
                        name: '',
                        password: '',
                        phone: '',
                        cui: ''
                    }
            }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLinkChange = this.handleLinkChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleCuiChange = this.handleCuiChange.bind(this);
        this.handleUpdatePress = this.handleUpdatePress.bind(this);

    }
    componentDidMount(){
        AccountService.getCompany().then((company:ICompanyRegister) => {
            this.setState({
                company:company
            });
        });
    }
    handleUpdatePress(event: any) {
        AccountService.updateCompany(this.state.company).then((resp) => {
            Alert.success("Modificari efectuate!", {
                position: 'top-right',
                effect: 'jelly'
            });
        });

    }


    handleEmailChange(event: any) {
        this.setState({
            company: update(this.state.company, {email: {$set: event.target.value}})
        });
    }

    handleAddressChange(event: any) {
        this.setState({
            company: update(this.state.company, {address: {$set: event.target.value}})
        });
    }

    handlePasswordChange(event: any) {
        this.setState({
            company: update(this.state.company, {password: {$set: event.target.value}})
        });
    }




    handleNameChange(event: any) {
        this.setState({
            company: update(this.state.company, {name: {$set: event.target.value}})
        });
    }

    handlePhoneChange(event: any) {
        this.setState({
            company: update(this.state.company, {phone: {$set: event.target.value}})
        });
    }

    handleLinkChange(event: any) {
        this.setState({
            company: update(this.state.company, {link: {$set: event.target.value}})
        });
    }

    handleCuiChange(event: any) {
        this.setState({
            company: update(this.state.company, {cui: {$set: event.target.value}})
        });
    }

    handleDescriptionChange(event: any) {
        this.setState({
            company: update(this.state.company, {description: {$set: event.target.value}})
        });
    }

    render() {
        return (

            <div>
                <h1>Editeaza Profil</h1>
                <div className={'centerEdit'}>
                    <FormGroup>
                        <Label htmlFor="company">Companie</Label>
                        <Input type="text" id="company" placeholder="Nume" onChange={this.handleNameChange} value={this.state.company.name}/>
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="cui">CUI</Label>
                        <Input type="text" id="cui" placeholder="CUI" onChange={this.handleCuiChange} value={this.state.company.cui}/>
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="address">Adresa</Label>
                        <Input type="text" id="address" placeholder="Adresa" onChange={this.handleAddressChange} value={this.state.company.address}/>
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" id="email" placeholder="email@g.com" onChange={this.handleEmailChange} value={this.state.company.email}/>
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="password">Parola</Label>
                        <Input type="password" id="password" placeholder="Parola" onChange={this.handlePasswordChange} value={this.state.company.password}/>
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="phone">Telefon</Label>
                        <Input type="text" id="phone" placeholder="00000000" onChange={this.handlePhoneChange} value={this.state.company.phone}/>
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="desc">Descriere</Label>
                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                               placeholder="Descriere..." onChange={this.handleDescriptionChange} value={this.state.company.description}/>
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="link">Link Pagina Companie</Label>
                        <Input type="text" id="link" placeholder="http://link.com" onChange={this.handleLinkChange} value={this.state.company.link}/>
                    </FormGroup>


                    <Button type="submit" size="sm" color="primary" onClick={this.handleUpdatePress}><i
                        className="fa fa-dot-circle-o" ></i> Submit</Button>

                </div>

            </div>


        );
    }
}
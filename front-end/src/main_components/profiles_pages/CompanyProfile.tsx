import * as React from "react";
import {AccountService} from "../../services/AccountService";
import { VBox, HBox } from 'react-stylesheet';
import "../../styles/scss/main.scss"
import 'coreui-react/assets/css/simple-line-icons.css';
import '@coreui/icons/css/coreui-icons.css'

import {ICompanyRegister} from "../../models/ICompanyRegister";
export interface CompanyProfileProps
{
}

interface CompanyProfileState {
    company:ICompanyRegister
}

export class CompanyProfile extends React.Component<CompanyProfileProps, CompanyProfileState> {
    constructor(props) {
        super(props);
        this.state =
            {
                company:
                    {
                        address: '',
                        email: '',
                        description: '',
                        link: '',
                        name: '',
                        password: '',
                        phone: '',
                        cui:''
                    }
            }

    }

    componentDidMount(){
        AccountService.getCompany().then((company:ICompanyRegister) => {
            this.setState({
                company:company
            });
        });
    }

    render() {
        return (

            <div><header>
                <h1>Profil Companie</h1>
                <h1>{this.state.company.name}</h1>
            </header>
                <VBox className='info'>

                    <label >
                        <b>CUI</b>
                    </label>

                    <label >
                        {this.state.company.cui}
                    </label>
                    <hr/>
                    <label >
                        <b>Link</b>
                    </label>

                    <label>
                        {this.state.company.link}
                    </label>
                    <hr/>
                    <label >
                        <b>Descriere</b>
                    </label>

                    <label>
                        {this.state.company.description}
                    </label>
                    <hr/>
                    <label >
                        <b>Adresa</b>
                    </label>

                    <label>
                        {this.state.company.address}
                    </label>
                    <hr/>
                    <label >
                        <b>Email</b>
                    </label>

                    <label>
                        {this.state.company.email}
                    </label>
                    <hr/>
                    <label >
                        <b>Telefon</b>
                    </label>

                    <label>
                        {this.state.company.phone}
                    </label>

                </VBox>
            </div>

        );
    }
}
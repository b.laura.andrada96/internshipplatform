import * as React from "react";
import {IStudentGet} from "../../models/IStudentGet";
import {AccountService} from "../../services/AccountService";
import { VBox, HBox } from 'react-stylesheet';
import "../../styles/scss/main.scss"
import 'coreui-react/assets/css/simple-line-icons.css';
import '@coreui/icons/css/coreui-icons.css'

export interface StudentProfileProps
{

}

interface StudentProfileState {
    student:IStudentGet
}

export class StudentProfile extends React.Component<StudentProfileProps, StudentProfileState> {
    constructor(props) {
        super(props);
        this.state={
            student:{
                name:'',
                email:'',
                password:'',
                university:'',
                specialization:'',
                faculty:'',
                age:0,
                city:'',
                county:'',
                phone:''
            }
        }
    }
    componentDidMount(){
        AccountService.getStudent().then((student:IStudentGet) => {
            this.setState({
                student:student
            });
        });
    }




    render() {

        return (

            <div><header>
                <h1>Profil Student</h1>

                <h1>{this.state.student.name}</h1>
            </header>
                <VBox className={"info"}>

                    <label htmlFor='university'>
                        <b>Universitate</b>
                    </label>

                    <label htmlFor='university'>
                        {this.state.student.university}
                    </label>
                    <hr/>
                    <label htmlFor='faculty'>
                        <b>Facultate</b>
                    </label>

                    <label htmlFor='faculty'>
                        {this.state.student.faculty}
                    </label>
                    <hr/>
                    <label htmlFor='specialization'>
                        <b>Specializare</b>
                    </label>

                    <label htmlFor='specialization'>
                        {this.state.student.specialization}
                    </label>
                    <hr/>
                    <label >
                        <b>Varsta</b>
                    </label>

                    <label>
                        {this.state.student.age}
                    </label>
                    <hr/>
                    <label >
                        <b>Oras</b>
                    </label>

                    <label>
                        {this.state.student.city}
                    </label>
                    <hr/>
                    <label >
                        <b>Judet</b>
                    </label>

                    <label>
                        {this.state.student.county}
                    </label>
                    <hr />
                    <label >
                        <b>Email</b>
                    </label>

                    <label>
                        {this.state.student.email}
                    </label>
                    <hr/>
                    <label >
                        <b>Telefon</b>
                    </label>

                    <label>
                        {this.state.student.phone}
                    </label>

                </VBox>
            </div>

        );
    }
}
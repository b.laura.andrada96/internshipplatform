import * as React from 'react';

import "../../node_modules/coreui-react/assets/css/style.css"
import "../styles/alignment.css";
import { VBox, HBox } from 'react-stylesheet';
import { IUserLogin } from '../Models/IUserLogin';
import { AccountService } from '../services/AccountService';
import update from 'react-addons-update';
import {DefaultLayout} from "../layout/DefaultLayout";
import {Redirect} from "react-router-dom";
export interface LoginProps
{
   // handleToParent:Function
}

interface LoginState {
    user: IUserLogin,
    role:string,
    isLogged: boolean
}

export class LogIn extends React.Component<LoginProps, LoginState> 
{
    constructor(props: LoginProps) {
        super(props);

        this.state =
            {
                user:
                    {
                        email: '',
                        password: '',
                    },

                role:'',
                isLogged:false
            }
    }

    handleEmailChange(event: any) {
        this.setState({
            user: update(this.state.user, { email: { $set: event.target.value } })
        });
    }
  
    handlePasswordChange(event: any) {
        this.setState({
            user: update(this.state.user, { password: { $set: event.target.value } })
        });
    }

    handleLogInPress(event: any) {
        console.log('logging in');
        AccountService.loginUser(this.state.user).then((resp) => {
            console.log('i m here');
            AccountService.getUserRole().then((resp)=>{
                console.log('Role:'+resp.data)
                this.setState({role:resp.data, isLogged: true});
               // this.props.handleToParent(this.state.isLogged,this.state.role);
            });
        });



    }


    render() {

            if (this.state.role == "ADMIN") {

                return <Redirect to={"/admin"}/>
            }

            if (this.state.role == "STUDENT") {
                return <Redirect to={"/student"}/>
            }

            if (this.state.role == "COMPANY") {
                return <Redirect to={"/company"}/>
            }

       return (
            <form className="center">
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Email</label>
                        <input type="text" className="form-control" id="formGroupExampleInput4" placeholder="Email" onChange={this.handleEmailChange.bind(this)}/>
                </div>
                
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Password</label>
                        <input type="password" className="form-control" id="formGroupExampleInput6" placeholder="Password" onChange={this.handlePasswordChange.bind(this)}/>
                </div>
            </VBox>

            <button type = "button" className="btn btn-primary btn-lg center-button" onClick={(event) => this.handleLogInPress(event)}>Log in</button>
            </form>
        );}

}

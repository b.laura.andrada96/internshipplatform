import * as React from 'react';

import "../../node_modules/coreui-react/assets/css/style.css"
import "../styles/alignment.css";
import { VBox, HBox } from 'react-stylesheet';
import { IStudentRegister } from '../Models/IStudentRegister';
import { AccountService } from '../services/AccountService';
import update from 'react-addons-update';
import Alert from 'react-s-alert';

export interface SignUpProps 
{
}

interface SignUpState {
    newUser: IStudentRegister
    created: boolean
}

export class SignUpStudent extends React.Component<SignUpProps, SignUpState> 
{
    constructor(props: SignUpProps) {
        super(props);

        this.state =
            {
                newUser:
                    {
                        age: 0,
                        city: '',
                        county:'',
                        email: '',
                        faculty: '',
                        name:'',
                        password: '',
                        phone: '',
                        specialization: '',
                        university: ''
                    },
                created: false,
            }
    }

    handleEmailChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { email: { $set: event.target.value } })
        });
    }

    handleCityChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { city: { $set: event.target.value } })
        });
    }

    handleCountyChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { county: { $set: event.target.value } })
        });
    }
 
    handlePasswordChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { password: { $set: event.target.value } })
        });
    }

    handleConfirmPasswordChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { confirmPassword: { $set: event.target.value } } )
        });
    }

    handleSignUpPress(event: any) {
        console.log('signing up');
        AccountService.registerStudent(this.state.newUser).then((resp) => {
            this.setState({
                created: update(this.state.created, { $set: true } )
            });
            Alert.success("A new account has been created!", {
                position: 'top-right',
                effect: 'jelly'
            });
        });
    }

    handleNameChange(event : any) {
        this.setState({
            newUser: update(this.state.newUser, { text: { $set: event.target.value } })
        });
    }

    handleAgeChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { age: { $set: event.target.value } })
        });
    }

    handleFacultyChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { faculty: { $set: event.target.value } })
        });
    }

    handlePhoneChange(event: any) {
        this.setState({
            newUser: update(this.state.newUser, { phone: { $set: event.target.value } })
        });
    }

    handleSpecializationChange(event : any) {
        this.setState({
            newUser: update(this.state.newUser, { specialization: { $set: event.target.value } })
        });
    }

    handleUniversityChange(event: any){
        this.setState({
            newUser: update(this.state.newUser, { university: { $set: event.target.value } })
        });
    }

    render() {
        return (
            <form className="center">
            <VBox id="signup-container">
            <HBox class="hbox-align">
                <VBox>
                <div className="form-group textbox-align">
                    <label htmlFor="formGroupExampleInput">Name</label>
                    <input type="text" className="form-control" id="formGroupExampleInput1" placeholder="Name" onChange={this.handleNameChange.bind(this)}/>
                </div>
                </VBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">Age</label>
                        <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Age" onChange={this.handleAgeChange.bind(this)}/>
                </div>
                </VBox>
            </HBox>
            <HBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">Phone</label>
                        <input type="text" className="form-control" id="formGroupExampleInput8" placeholder="Phone" onChange={this.handlePhoneChange.bind(this)}/>
                </div>
                </VBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">City</label>
                        <input type="text" className="form-control" id="formGroupExampleInput3" placeholder="City" onChange={this.handleCityChange.bind(this)}/>
                </div>
                </VBox>

                <VBox>
                    <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">County</label>
                        <input type="text" className="form-control" id="formGroupExampleInput3" placeholder="County" onChange={this.handleCountyChange.bind(this)}/>
                    </div>
                </VBox>
            </HBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Email</label>
                        <input type="text" className="form-control" id="formGroupExampleInput4" placeholder="Email" onChange={this.handleEmailChange.bind(this)}/>
                </div>
                
            </VBox>
            <HBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">University</label>
                        <input type="text" className="form-control" id="formGroupExampleInput10" placeholder="University" onChange={this.handleUniversityChange.bind(this)}/>
                </div>
                </VBox>
                <VBox>
                <div className="form-group; textbox-align">
                        <label htmlFor="formGroupExampleInput">Faculty</label>
                        <input type="text" className="form-control" id="formGroupExampleInput5" placeholder="Faculty" onChange={this.handleFacultyChange.bind(this)}/>
                </div>
                </VBox>
            </HBox>
            <VBox  class="vbox-size">
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Specialization</label>
                        <input type="text" className="form-control" id="formGroupExampleInput9" placeholder="Specialization" onChange={this.handleSpecializationChange.bind(this)}/>
                </div>
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Password</label>
                        <input type="password" className="form-control" id="formGroupExampleInput6" placeholder="Password" onChange={this.handlePasswordChange.bind(this)}/>
                </div>
            </VBox>
            <VBox>
                <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Confirm Password</label>
                        <input type="password" className="form-control" id="formGroupExampleInput7" placeholder="Confirm Password" onChange={this.handleConfirmPasswordChange.bind(this)}/>
                </div>
            </VBox>
            </VBox>
            <button type = "button" className="btn btn-primary btn-lg center-button" onClick={(event) => this.handleSignUpPress(event)}>Sign up</button>
            </form>
            
        );
    }
}

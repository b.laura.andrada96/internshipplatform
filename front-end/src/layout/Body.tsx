import * as React from 'react';
import {Route,Switch} from 'react-router-dom';
import {LogIn} from "../main_components/Login";
import {SignUp} from "../main_components/SignUp";
import {SignUpCompany} from "../main_components/SignUpCompany";
import {SignUpStudent} from "../main_components/SignUpStudent";
import {DefaultLayout} from "./DefaultLayout";
import {StartPage} from "./StartPage";

export interface BodyProps{
   // role:string;
}
interface BodyState{}

export class Body extends React.Component<BodyProps,BodyState>
{
    constructor(props)
    {
        super(props);
        this.state={};

    }

    render()
    {




        return(
            <Switch>
            <div>

                <Route path="/" exact component={props => <StartPage/>}/>
                <Route path="/login" exact component={props => <LogIn/>}/>
                <Route path="/signup" component={props => <SignUp/>}/>
                <Route path={"/signupStudent"} component={props=><SignUpStudent/>}/>
                <Route path={"/signupCompany"} component={props=><SignUpCompany/>}/>
                <Route path={"/student"} component={props=><DefaultLayout role={"STUDENT"}/>}/>
                <Route path={"/company"} component={props=><DefaultLayout role={"COMPANY"}/>}/>
                <Route path={"/admin"} component={props=><DefaultLayout role={"ADMIN"}/>}/>

            </div>
            </Switch>
        )
    }



}
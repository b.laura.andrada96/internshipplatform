import * as React from "react";

import { VBox, HBox } from 'react-stylesheet';
import {Redirect} from 'react-router-dom';
import "../styles/alignment.css";
import cookie from 'react-cookies';
import {AccountService} from "../services/AccountService";
export interface StartPageProps
{
}

interface StartPageState {
    toLogin:boolean,
    toRegister:boolean,
    isLoggedIn:boolean,
    role:string
}

export class StartPage extends React.Component<StartPageProps, StartPageState> {
    constructor(props) {
        super(props);
        this.handleToLoginPressed= this.handleToLoginPressed.bind(this);
        this.handleToRegisterPressed= this.handleToRegisterPressed.bind(this);
        this.state={toLogin:false,toRegister:false,isLoggedIn:false,role:''};
    }

    componentDidMount(){
        if(cookie.load('token')){
            AccountService.getUserRole().then((resp)=>{
                console.log('Role:'+resp.data)
                this.setState({role:resp.data, isLoggedIn: true});
            });

        }
    }

    handleToLoginPressed(event){
        this.setState({toLogin:true,toRegister:false});
    }

    handleToRegisterPressed(event){
        this.setState({toLogin:false,toRegister:true});
    }
    render() {

        if(this.state.isLoggedIn){
            if(this.state.role == 'STUDENT'){
                return(<Redirect to={'/student'} />);
            }
            if(this.state.role == 'ADMIN'){
                return(<Redirect to={'/admin'} />);
            }
            if(this.state.role == 'COMPANY'){
                return(<Redirect to={'/company'} />);
            }
        }
        if(this.state.toLogin){
            return(<Redirect to={'/login'} />);
        }
        else if(this.state.toRegister){
            return(<Redirect to={'/signup'} />);
        }
        return (
                <div className={"startPage"}>
                <div className="centerStart">
                    <VBox>
                        <button type = "button" className="btn btn-primary btn-lg center-buttonStart" onClick={(event) => this.handleToLoginPressed(event)}>Login</button>

                    </VBox>

                    <VBox>
                        <button type = "button" className="btn btn-primary btn-lg center-buttonStart" onClick={(event) => this.handleToRegisterPressed(event)}>Creeaza Cont</button>
                    </VBox>
                </div>
                </div>

        );
    }
}
import React, { Component } from 'react';
import {BodyLayout, BodyLayoutProps, BodyLayoutState} from "./BodyLayout";
import {Header} from "./Header";


export interface DefaultLayoutProps
{
    role:string
}

export interface DefaultLayoutState {


    //role:string
}

export class DefaultLayout extends React.Component<DefaultLayoutProps, DefaultLayoutState> {
    constructor(props) {
        super(props);
        this.state={isLogged:false}//, role:'NO_ROLE'}
        //this.handleFromChild=this.handleFromChild.bind(this);
    }




    // handleFromChild(isLoggedIn, childRole){
    //     this.setState({isLogged:isLoggedIn, role:childRole})
    // }

    render(){
       // if(this.state.isLogged)
        //{
            return(
            <div>
                <Header />
                <BodyLayout role= {this.props.role}/>
            </div>
        )
                // ;}
        // return(
        //   <HomeLayout handleToParent={this.handleFromChild}/>
        // );
    }

}
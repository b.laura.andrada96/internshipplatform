import * as React from "react";
import {HashRouter as Router, Route} from 'react-router-dom'
import 'coreui-react/assets/css/simple-line-icons.css';
import '@coreui/icons/css/coreui-icons.css'
import { Card,  CardHeader, CardBody, CardText } from 'reactstrap';
import {Button,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Badge} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {AccountService} from "../services/AccountService";

export interface HeaderProps
{

}

interface HeaderState {
    isOpenNotif:boolean,
    isOpenUser:boolean,
    count:number
}

export class Header extends React.Component<HeaderProps, HeaderState> {
    constructor(props) {
        super(props);
        this.toggleNotif = this.toggleNotif.bind(this);
        this.toggleUser = this.toggleUser.bind(this);
        this.state = {
            isOpenNotif: false, isOpenUser:false,count:0       };
    }
    toggleNotif() {
        this.setState({
            isOpenNotif: !this.state.isOpenNotif
        });
    }
    toggleUser() {
        this.setState({
            isOpenUser: !this.state.isOpenUser
        });
    }

    logoutUser(){
        AccountService.logoutUser().then((resp)=>{
            console.log('Role:'+resp.data)
        });
    }

    render(){
        return(
            <Router>
            <div style={{position:'relative'}}>

                    <Navbar color="light" light expand="md">
                        <NavbarToggler onClick={this.toggleNotif} />
                        <Collapse isOpen={this.state.isOpenNotif} navbar>
                            <Nav className="ml-auto" navbar>

                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        <i className='icon-bell' style={{ fontSize: '1.2em'}} /> <Badge pill color="danger">{this.state.count>0?this.state.count:0}</Badge>
                                    </DropdownToggle>
                                    <div style={{width:'20%',height:'10%'}}>
                                    <DropdownMenu right>
                                        <DropdownItem>
                                            <b>Notif1</b><br/>
                                            <p>tehkskajdksjk</p>



                                        </DropdownItem>
                                        <DropdownItem>
                                            <b>Notif1</b><br/>
                                            <p>tehkskajdksjk</p>
                                        </DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem>
                                            Vezi toate notificarile
                                        </DropdownItem>
                                    </DropdownMenu></div>
                                </UncontrolledDropdown>
                                <NavLink href="/"><Button onClick={this.logoutUser} color="info">
                                    Logout
                                </Button></NavLink>
                            </Nav>
                        </Collapse>
                    </Navbar>
            </div>
            </Router>
        );
    }
}

import {HashRouter as Router, Route} from 'react-router-dom'

import React, {Component} from 'react';
import styled from 'styled-components';
import SideNav, {Toggle, NavItem, NavIcon, NavText} from '@trendmicro/react-sidenav';

//import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import '../styles/trendimicro_sidenav.css'

import 'coreui-react/assets/css/simple-line-icons.css';
import '@coreui/icons/css/coreui-icons.css'
import pageList from '../_nav';
import {Dashboard} from "../main_components/sidepanel_pages/Dashboard";
import {MyApplications} from "../main_components/sidepanel_pages/MyApplications";
import {Announcements} from "../main_components/sidepanel_pages/Announcements";
import {Companies} from "../main_components/sidepanel_pages/Companies";
import {Statistics} from "../main_components/sidepanel_pages/Statistics";
import {Students} from "../main_components/sidepanel_pages/Students";
import {MyApplicants} from "../main_components/sidepanel_pages/MyApplicants";
import {StudentProfile} from "../main_components/profiles_pages/StudentProfile";
import {EditStudentProfile} from "../main_components/profiles_pages/EditStudentProfile";
import {EditAdminProfile} from "../main_components/profiles_pages/EditAdminProfile";
import {AdminProfile} from "../main_components/profiles_pages/AdminProfile";
import {CompanyProfile} from "../main_components/profiles_pages/CompanyProfile";
import {EditCompanyProfile} from "../main_components/profiles_pages/EditCompanyProfile";

const Main = styled.main`
    
    position: relative;
    overflow: hidden;
    transition: all .15s;
    padding: 0 20px;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
    margin-bottom:10px;
`;

export interface BodyLayoutProps {
    role: string
}

export interface BodyLayoutState {
    selected: string,
    expanded: false
}

export class BodyLayout extends React.Component<BodyLayoutProps, BodyLayoutState> {
    constructor(props) {
        super(props);
        this.state = {selected: 'dashboard', expanded: false};

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle = (expanded) => {
        this.setState({expanded: expanded});
    };

    pageCurrentList = pageList[this.props.role];

    navigate = (pathname) => () => {
        this.setState({selected: pathname});
    };

    render() {
        console.log(this.pageCurrentList[0]['url'])

        var navlist = [];
        for (var i = 0; i < this.pageCurrentList.length; i++) {
            if (i == 1) {
                navlist.push(

                    <NavItem eventKey="profile">
                        <NavIcon>
                            <i className='icon-user'
                               style={{fontSize: '1.75em', verticalAlign: 'middle'}}/>
                        </NavIcon>
                        <NavText style={{paddingRight: 32, fontSize: '1.2em', fontFamily: 'Arial'}}
                                 title='Profil'>
                            {'Profil'}
                        </NavText>

                        <NavItem eventKey={this.pageCurrentList[i]['url']}>

                            <NavText style={{paddingRight: 32, fontSize: '1.2em', fontFamily: 'Arial'}}
                                     title={this.pageCurrentList[i]['text']}>
                                {this.pageCurrentList[i]['text']}
                            </NavText>
                        </NavItem>
                        <NavItem eventKey={this.pageCurrentList[i+1]['url']}>

                            <NavText style={{paddingRight: 32, fontSize: '1.2em', fontFamily: 'Arial'}}
                                     title={this.pageCurrentList[i+1]['text']}>
                                {this.pageCurrentList[i+1]['text']}
                            </NavText>
                        </NavItem>
                    </NavItem>

                );
                i++;
            }
            else {
                navlist.push(<NavItem eventKey={this.pageCurrentList[i]['url']}>
                    <NavIcon>
                        <i className={this.pageCurrentList[i]['icon']}
                           style={{fontSize: '1.75em', verticalAlign: 'middle'}}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32, fontSize: '1.2em', fontFamily: 'Arial'}}
                             title={this.pageCurrentList[i]['text']}>
                        {this.pageCurrentList[i]['text']}
                    </NavText>
                </NavItem>);
            }
        }
        console.log(navlist.length);
        return (
            <Router>
                <Route render={({location, history}) => (

                    <div>


                        <SideNav onSelect={(selected) => {
                            this.setState({selected: selected});
                            const to = '/' + selected;
                            if (location.pathname !== to) {
                                history.push(to);
                            }
                        }} onToggle={this.handleToggle}>
                            <SideNav.Toggle/>
                            <SideNav.Nav>
                                {navlist}
                            </SideNav.Nav>
                        </SideNav>


                        <Main expanded={this.state.expanded}>

                            <Route path="/dashboard" exact component={props => <Dashboard/>}/>
                            <Route path="/myapplications" component={props => <MyApplications/>}/>
                            <Route path="/myapplicants" component={props => <MyApplicants/>}/>
                            <Route path="/announcements" component={props => <Announcements/>}/>
                            <Route path="/companies" component={props => <Companies/>}/>
                            <Route path="/students" component={props => <Students/>}/>
                            <Route path="/statistics" component={props => <Statistics/>}/>
                            <Route path="/student_profile/view" component={props => <StudentProfile/>}/>
                            <Route path="/student_profile/edit" component={props => <EditStudentProfile/>}/>
                            <Route path="/company_profile/view" component={props => <CompanyProfile/>}/>
                            <Route path="/company_profile/edit" component={props => <EditCompanyProfile/>}/>
                            <Route path="/admin_profile/view" component={props => <AdminProfile/>}/>
                            <Route path="/admin_profile/edit" component={props => <EditAdminProfile/>}/>

                        </Main>


                    </div>

                )}
                />
            </Router>
        );
    }
}
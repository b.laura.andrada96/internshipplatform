import * as React from "react";
import * as ReactDOM from "react-dom";

import { LogIn } from "./main_components/Login";
import { SignUpCompany } from "./main_components/SignUpCompany";
import {App} from "./App";
ReactDOM.render(
    <App/>,
    document.getElementById('root') as HTMLElement
);
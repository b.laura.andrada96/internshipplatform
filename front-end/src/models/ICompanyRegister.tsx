export interface ICompanyRegister {
    address: string,
    email: string,
    description: string,
    link: string,
    name: string,
    password: string,
    phone: string,
    cui:string
}
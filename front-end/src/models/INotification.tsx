export interface INotification{
    from:string,
    message:string,
    date:Date
}
export interface IStudentRegister {
    age: number,
    city: string,
    county:string,
    email: string,
    faculty: string,
    name: string,
    password: string,
    phone: string,
    specialization: string,
    university: string
}
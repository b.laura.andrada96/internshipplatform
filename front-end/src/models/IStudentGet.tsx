export interface IStudentGet{
    name:string,
    email:string,
    password:string,
    university:string,
    specialization:string,
    faculty:string,
    age:number,
    city:string,
    county:string,
    phone:string
}
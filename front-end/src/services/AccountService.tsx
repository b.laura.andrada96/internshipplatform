import axios from 'axios';
import { Config } from '../UrlConfig';
import {IStudentGet} from "../models/IStudentGet";
import {ICompanyRegister} from "../models/ICompanyRegister";
import {ISiteAdmin} from "../models/ISiteAdmin";
export class AccountService {

    private static root: string = Config.url;

    public static registerStudent(newUser: any) : Promise<any>  {
        console.log(newUser);
        return new Promise((resolve, reject) => {
            axios(
                this.root + '/sign-up/student',
                {
                    method: 'PUT',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                    data: newUser
                }
            ).then((response: any) => {
                console.log(response);
                console.log(response.headers['set-cookie']);
                console.log(document.cookie);
                resolve(response);
            },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    public static registerCompany(newUser: any) : Promise<any>  {
        console.log(newUser);
        return new Promise((resolve, reject) => {
            axios(
                this.root + '/sign-up/company',
                {
                    method: 'PUT',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                    data: newUser
                }
            ).then((response: any) => {
                console.log(response);
                console.log(response.headers['set-cookie']);
                console.log(document.cookie);
                resolve(response);
            },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    public static loginUser(user: any) : Promise<any>  {
        console.log(user);
        return new Promise((resolve, reject) => {
            axios(
                this.root + '/login',
                {
                    method: 'POST',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                    data: user
                }
            ).then((response: any) => {
                console.log(response);
                console.log(response.headers['set-cookie']);
                console.log(document.cookie);
                resolve(response);
            },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    public static logoutUser() : Promise<any>  {

        return new Promise((resolve, reject) => {
            axios(
                this.root + '/logout',
                {
                    method: 'POST',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0
                }
            ).then((response: any) => {
                    console.log(response);
                    console.log(response.headers['set-cookie']);
                    console.log(document.cookie);
                    resolve(response);
                },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    public static getUserRole() : Promise<any>  {
        return new Promise((resolve, reject) => {
            axios(
                this.root + '/roles',
                {
                    method: 'GET',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                }
            ).then((response: any) => {
                    console.log(response);
                    console.log(response.headers['set-cookie']);
                    console.log(document.cookie);

                    resolve(response);
                },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }



    public static updateStudent(student: IStudentGet) : Promise<any> {

        return new Promise((resolve, reject) => {
            axios(
                this.root + '/students',
                {
                    method: 'PUT',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin': 'http://localhost:3000',
                        'Content-Type': 'application/json'
                    },

                    maxRedirects: 0,
                    data: student
                }
            ).then((response: any) => {
                    console.log(response);
                    console.log(response.headers['set-cookie']);
                    console.log(document.cookie);
                    resolve(response);
                },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    public static updateCompany(company:ICompanyRegister) : Promise<any>  {

        return new Promise((resolve, reject) => {
            axios(
                this.root + '/companies',
                {
                    method: 'PUT',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                    data: company
                }
            ).then((response: any) => {
                    console.log(response);
                    console.log(response.headers['set-cookie']);
                    console.log(document.cookie);
                    resolve(response);
                },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    public static updateAdmin(admin:ISiteAdmin) : Promise<any>  {

        return new Promise((resolve, reject) => {
            axios(
                this.root + '/admins',
                {
                    method: 'PUT',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                    data: admin
                }
            ).then((response: any) => {
                    console.log(response);
                    console.log(response.headers['set-cookie']);
                    console.log(document.cookie);
                    resolve(response);
                },
                (error: any) => {
                    console.log(error);
                    reject(error);
                });
        });
    }



    // public static getCloudinaryResponse(api) : Promise<any>  {
    //     return new Promise((resolve, reject) => {
    //         axios(
    //             api,
    //             {
    //                 method: 'GET'
    //             }
    //         ).then((response: any) => {
    //                 console.log(response);
    //                 resolve(response);
    //             },
    //             (error: any) => {
    //                 console.log(error.status);
    //                 reject(error);
    //             });
    //     });
    // }

    //todo
    public static getStudent(): Promise<IStudentGet> {
        return new Promise((resolve, reject) => {
            axios(
                this.root+"/student",
                {
                    method: 'GET',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                }
            ).then((response: any) => {
                    //let doctor = response.data.map(this.toDoctor);
                    let student={
                        name:response.data.name,
                        email:response.data.email,
                        password:response.data.password,
                        university:response.data.university,
                        specialization:response.data.specialization,
                        faculty:response.data.faculty,
                        age:response.data.age,
                        city:response.data.city,
                        county:response.data.county,
                        phone:response.data.phone
                    };
                    resolve(student);
                },
                (error: any) => {
                    reject(error);
                });
        });
    }


    public static getCompany(): Promise<ICompanyRegister> {
        return new Promise((resolve, reject) => {
            axios(
                this.root+"/company",
                {
                    method: 'GET',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                }
            ).then((response: any) => {

                    let company={
                        name:response.data.name,
                        email:response.data.email,
                        password:response.data.password,
                        address:response.data.address,
                        cui:response.data.cui,
                        description:response.data.description,
                        link:response.data.link,
                        phone:response.data.phone
                    };
                    resolve(company);
                },
                (error: any) => {
                    reject(error);
                });
        });
    }

    public static getAdmin(): Promise<ISiteAdmin> {
        return new Promise((resolve, reject) => {
            axios(
                this.root+"/admin",
                {
                    method: 'GET',
                    withCredentials: true,
                    headers: {
                        'Access-Control-Allow-Origin' : 'http://localhost:3000',
                        'Content-Type':'application/json'
                    },

                    maxRedirects: 0,
                }
            ).then((response: any) => {

                    let admin={

                        email:response.data.email,
                        password:response.data.password,

                    };
                    resolve(admin);
                },
                (error: any) => {
                    reject(error);
                });
        });
    }







}